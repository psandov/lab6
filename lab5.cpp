/*******************
  Patrick Sandoval
  psandov
  Lab 5
  Lab Section: 5
  TA: Alex Myers
*******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

/* Struct for the cards so they have a value and a suit */
typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i; }

int main(int argc, char const *argv[]) {
  /* This is to seed the random generator */
  srand(unsigned (time(0)));
  /* Creates an array of 52 cards of type "Card" */
    /* Each card has a number and suit */
   Card deck[52];
   int type = 0;
   int k = 0;
   for (int i = 0; i < 4; ++i) {
     for (int j = 2; j < 15; ++j) {
       deck[k].value = j;
       deck[k].suit = Suit(type);
       ++k;
     }
     ++type;
   }

   /* Creates pointers to the begining and end of the deck */
   Card *deckStart = deck;
   Card *deckEnd = &deck[52];
   /* Shuffles the deck */
   int (*function)(int) = myrandom;
   random_shuffle(deckStart, deckEnd, function);

    /* Creates a new array of the first 5 cards in the shuffled deck */
    Card hand[5];
    for (int i = 0; i < 5; ++i) {
        hand[i] = deck[i];
    }

     /* Creates pointers to the begining and end of the hand */
       /* Uses them and the "suit_order" function to sort the hand */
     Card *first = hand;
     Card *last = &hand[5];
     sort(first, last, suit_order);

     /* Prints out the hand neatly */
     for (int i = 0; i < 5; ++i) {
       cout << setw(10) << get_card_name(hand[i]) << " of ";
       cout << get_suit_code(hand[i]) << endl;
     }

  return 0;
}

/* Used to compare the suits of each card in the hand */
  /* This function is used in the "sort" functions */
  /* "lhs" and "rhs" are the two cards being sorted */
    /* The "lower" card gets put first in the hand */
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit) {
    return true;
  }
  if (lhs.suit == rhs.suit) {
    if(lhs.value < rhs.value) {
      return true;
    }
    else {
        return false;
    }
  }
  else {
    return false;
  }
}

/* Gets the unicode string of the suit to print out */
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

/* Used to get the card name/number */
string get_card_name(Card& c) {
    switch (c.value) {
      case 2:     return "2";
      case 3:     return "3";
      case 4:     return "4";
      case 5:     return "5";
      case 6:     return "6";
      case 7:     return "7";
      case 8:     return "8";
      case 9:     return "9";
      case 10:    return "10";
      case 11:    return "Jack";
      case 12:    return "Queen";
      case 13:    return "King";
      case 14:    return "Ace";
      default:    return "";
    }
}
